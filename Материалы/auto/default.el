(TeX-add-style-hook "default"
 (lambda ()
    (LaTeX-add-labels
     "indexsec"
     "MyLastPage")
    (TeX-add-symbols
     "contentsname"
     "leftmark"
     "indexname")
    (TeX-run-style-hooks
     "preamble"
     "title/title"
     "text/mtt"
     "text/ngn"
     "text/it"
     "text/parallel"
     "text/model"
     "title/backpage")))

